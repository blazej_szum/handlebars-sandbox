import './styles.scss'

import Swiper from 'swiper'
import * as Handlebars from './../node_modules/handlebars/dist/handlebars.min'

import persons from './persons.js'


const template = Handlebars.compile(`
<div class="header">Meet our team</div>
<di class="person-container">
    {{#each persons}}
    <div class="person">
        <img class="person__icon" width="210px" height="210px" src="https://source.unsplash.com/random/300x300?for={{name}}">
        <div class="person__name">
            {{name}}
        </div>
        <div class="person__role">
            {{role}}
        </div>
        <div class="person__location">
            {{location}}
        </div>
    </div>
    {{/each}}
</di>


<div class="header">Swype our team!</div>
<div class="swiper-container">
    <div class="swiper-wrapper">
		{{#each persons}}
		<div class="swiper-slide">
			<div class="person">
				<img class="person__icon" width="210px" height="210px" src="https://source.unsplash.com/random/300x300?for={{name}}">
				<div class="person__name">
					{{name}}
				</div>
				<div class="person__role">
					{{role}}
				</div>
				<div class="person__location">
					{{location}}
				</div>
			</div>
		</div>
		{{/each}}
	</div>
    <div class="swiper-pagination"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
</div>
`);
document.getElementById('member-list-handlebars').innerHTML = template({persons});

new Swiper('.swiper-container', {
    speed: 400,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
    },
    slidesPerView: 2,
    effect: 'coverflow',
    grabCursor: true,
    centeredSlides: true,
    coverflowEffect: {
        rotate: -30,
        stretch: 0,
        depth: 200,
        modifier: 1,
        slideShadows : false,
    },
});
